// Antonio La Rocca
// 1000801000

package com.utm.csc;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class HTMLParser {

	public static void main(String[] args) {
		for(int i = 0; i < args.length; i++) {
            parseHTML(args[i]);
        }
	}
	
	private static void parseHTML (String source){
		try {
			// Connect to the webpage
			Document webpage = Jsoup.connect(source).timeout(0).get();
			// Print out any titles that the webpage contains
			Elements titles = webpage.getElementsByTag("title");
			for (Element title : titles){
				System.out.println("title: " + title.text());
				System.out.println();
			}
			// Print out any links the webpage contains
			Elements links = webpage.getElementsByTag("a");
			for (Element link : links) {
				// Filter out blank links
				if (link.attr("href") != "" && link.text() != ""){
					System.out.println("link: " + link.attr("href"));
					System.out.println("text: " + link.text());
					System.out.println();
				}
			}
		} catch (IOException e) {
			// Print out the error
			e.printStackTrace();
		}
	}
}
